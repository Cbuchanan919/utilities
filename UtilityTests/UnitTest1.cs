using Utilities;
using NUnit.Framework;
using System; 

namespace UtilityTests
{
    public class Tests
    {
        GenPurpose gen = null;
        [SetUp]
        public void Setup()
        {
            gen = new GenPurpose();
        }

        [Test]
        public void DayOfWeekTest_Monday()
        {
            DateTime testDate = gen.GetDateOfWeekday(DayOfWeek.Monday, new DateTime(2020, 10, 14));
            Assert.AreEqual(testDate, new DateTime(2020, 10, 12));
            //Assert.Pass();
        }
        [Test]
        public void DayOfWeekTest_Tuesday()
        {
            DateTime testDate = gen.GetDateOfWeekday(DayOfWeek.Tuesday, new DateTime(2020, 10, 11));
            Assert.AreEqual(testDate, new DateTime(2020, 10, 13));
        }
        [Test]
        public void DayOfWeekTest_Friday()
        {
            DateTime testDate = gen.GetDateOfWeekday(DayOfWeek.Friday, new DateTime(2020, 10, 16));
            Assert.AreEqual(testDate, new DateTime(2020, 10, 16));
        }
        [Test]
        public void DayOfWeekTest_Sunday()
        {
            DateTime testDate = gen.GetDateOfWeekday(DayOfWeek.Sunday, new DateTime(2020, 10, 17));
            Assert.AreEqual(testDate, new DateTime(2020, 10, 11));
        }
        [Test]
        public void DayOfWeekTest_Today()
        {
            DateTime testDate = gen.GetDateOfWeekday(DateTime.Today.DayOfWeek, DateTime.Today);
            Assert.AreEqual(testDate, DateTime.Today);
        }
    }
}