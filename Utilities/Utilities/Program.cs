﻿using System;

namespace Utilities
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            GenPurpose gen = new GenPurpose();
            Console.WriteLine("This week's Monday is:");
            Console.WriteLine(gen.GetDateOfWeekday(DayOfWeek.Monday, DateTime.Today).ToString());
            
        }


    }

    public class GenPurpose
    {
        public GenPurpose()
        {
            //currently blank. 
        }

        /// <summary>
        /// Returns the date of the day of week of the given date. 
        /// </summary>
        /// <param name="dayOfWeek">The DayOfWeek desired. ie Monday</param>
        /// <param name="d">The starting date</param>
        /// <returns></returns>
        public DateTime GetDateOfWeekday(DayOfWeek dayOfWeek, DateTime d)
        {
            int DayDiff = d.DayOfWeek - dayOfWeek;
            return d.AddDays(-DayDiff);
            
        }

        public override string ToString()
        {

            return base.ToString();
        }
    }
}
